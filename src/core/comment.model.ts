export interface IComment {
    id: string;
    published: Date;
    content: string;
}