import { IComment } from "./comment.model";


export interface IBlogpost {
    id: string;
    name: string;
    email: string;
    title: string;
    content: string;
    published: Date;
    comments: IComment[];
}