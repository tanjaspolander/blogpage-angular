export interface IMessage {
    id: string;
    content: string;
    username: string;
}