import { Component } from '@angular/core';
import { NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { BlogPageComponent } from './components/blog-page/blog-page.component';
import { HeaderComponent } from './components/header/header.component';
import { ChatHeaderComponent } from './components/chat-header/chat-header.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, BlogPageComponent, HeaderComponent, ChatHeaderComponent ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'blog';



}
