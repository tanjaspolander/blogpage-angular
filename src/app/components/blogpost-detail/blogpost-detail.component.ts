import { Component, inject } from '@angular/core';
import { IBlogpost } from '../../../core/blogpost.model';
import { BlogpostService } from '../../service/blogpost.service';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { IComment } from '../../../core/comment.model';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-blogpost-detail',
  standalone: true,
  imports: [RouterLink, FormsModule],
  templateUrl: './blogpost-detail.component.html',
  styleUrl: './blogpost-detail.component.css'
})
export class BlogpostDetailComponent {

  blogpostService: BlogpostService = inject(BlogpostService);

  blogpost: IBlogpost | undefined;
  comments: IComment[] = [];
  newComment: string = '';
  id: string;
  


  updateBlogpost = () => {
    this.blogpostService.getBlogpostById(this.id).then((blogpost: IBlogpost) => {
      this.blogpost = blogpost;
    });
  }

  constructor(blogpostService: BlogpostService, activatedRoute: ActivatedRoute) {
    this.id = activatedRoute.snapshot.params['id'];
    this.updateBlogpost();
  }


  ngOnInit(): void {
    this.blogpostService.reload.subscribe(() => {
      this.updateBlogpost();
    })

  }

  postComment = () => {  
    const comment: IComment = {
      id: uuidv4(),
      content: this.newComment,
      published: new Date()
    }
    this.blogpostService.addComment(this.blogpost!.id, comment);
    console.log("Updated Blog Post:", this.blogpost);

  }


}
