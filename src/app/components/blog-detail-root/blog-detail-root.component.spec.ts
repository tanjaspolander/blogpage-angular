import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogDetailRootComponent } from './blog-detail-root.component';

describe('BlogDetailRootComponent', () => {
  let component: BlogDetailRootComponent;
  let fixture: ComponentFixture<BlogDetailRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BlogDetailRootComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BlogDetailRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
