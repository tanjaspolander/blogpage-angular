import { Component } from '@angular/core';
import { BlogpostDetailComponent } from '../blogpost-detail/blogpost-detail.component';
import { HeaderComponent } from '../header/header.component';
import { HeaderDetailComponent } from '../header-detail/header-detail.component';

@Component({
  selector: 'app-blog-detail-root',
  standalone: true,
  imports: [BlogpostDetailComponent, HeaderDetailComponent],
  templateUrl: './blog-detail-root.component.html',
  styleUrl: './blog-detail-root.component.css'
})
export class BlogDetailRootComponent {

}
