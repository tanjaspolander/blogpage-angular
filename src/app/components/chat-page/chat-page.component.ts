import { Component, inject, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Observable, Subject, Subscription, interval, switchMap } from 'rxjs';
import { ChatService } from '../../service/chat.service';

import { CommonModule } from '@angular/common';
import { v4 as uuidv4 } from 'uuid';
import { IMessage } from '../../../core/message.model';



@Component({
  selector: 'app-chat-page',
  standalone: true,
  imports: [ReactiveFormsModule, FormsModule, CommonModule],
  templateUrl: './chat-page.component.html',
  styleUrl: './chat-page.component.css'
})
export class ChatPageComponent implements OnInit{



  reload = new Subject<void>();


  chatService: ChatService = inject(ChatService);
  username: string = '';
  newMessage: string = '';
  messages: IMessage[] = [];
  pollSubscription: Subscription | undefined;
  
  ngOnInit(): void {
    this.startPolling();
   }

   startPolling(): void {
    this.pollSubscription = interval(1000)
    .pipe(
      switchMap(() => this.chatService.getMessages())
    )
    .subscribe((messages: IMessage[]) => {
      this.messages = messages;
      console.log('Polling for messages...');
      console.log('Fetched messages:', messages); 
   });
   }


   stopPolling(): void {
    if (this.pollSubscription) {
      this.pollSubscription.unsubscribe();
    }
   }
  

     
sendMessage(): void {
  // lagg till validering
  this.chatService.addMessage(this.username, this.newMessage).subscribe(() => {
    console.log("Sender: ", this.username, "Message: ", this.newMessage);
    console.log("Messages: ", this.messages);
    this.getMessages();

  });
}

getMessages(): void {
  this.chatService.getMessages().subscribe((messages: IMessage[]) => {
    this.messages = messages;
  });
}


}
