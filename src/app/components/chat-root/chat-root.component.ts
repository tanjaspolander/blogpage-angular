import { Component } from '@angular/core';
import { ChatHeaderComponent } from '../chat-header/chat-header.component';
import { ChatPageComponent } from '../chat-page/chat-page.component';

@Component({
  selector: 'app-chat-root',
  standalone: true,
  imports: [ChatHeaderComponent, ChatPageComponent],
  templateUrl: './chat-root.component.html',
  styleUrl: './chat-root.component.css'
})
export class ChatRootComponent {

}
