import { Component, Input, inject } from '@angular/core';
import { BlogpostService } from '../../../service/blogpost.service';
import { IBlogpost } from '../../../../core/blogpost.model';
import { RouterLink } from '@angular/router';
import { v4 as uuidv4 } from 'uuid';


@Component({
  selector: 'app-posts',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './posts.component.html',
  styleUrl: './posts.component.css'
})
export class PostsComponent {



  @Input() blogpost: IBlogpost = { 
    id: uuidv4(),
    name: '',
    email: '',
    title: '',
    content: '',
    published: new Date(),
    comments: []
}

  blogpostService: BlogpostService = inject(BlogpostService);

  
 
}
