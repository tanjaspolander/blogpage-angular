import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BlogpostService } from '../../../service/blogpost.service';
import { IBlogpost } from '../../../../core/blogpost.model';
import { v4 as uuidv4 } from 'uuid';


@Component({
  selector: 'app-form',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './form.component.html',
  styleUrl: './form.component.css'
})
export class FormComponent {

  formBuilder: FormBuilder;
  blogPostForm;
 


  blogpostService: BlogpostService = inject(BlogpostService);
  

  constructor(formBuilder: FormBuilder) {
    this.formBuilder = formBuilder;
    this.blogPostForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      title: ['', Validators.required],
      content: ['', [Validators.required, Validators.minLength(8)]]
    });
  }


  onSubmit = () => {
   
    if (this.blogPostForm.valid) {
      const newBlogpost: IBlogpost = {
        id: uuidv4(),
        name: this.blogPostForm.value.name!,
        email: this.blogPostForm.value.email!,
        title: this.blogPostForm.value.title!,
        content: this.blogPostForm.value.content!,
        published: new Date(),
        comments: []
      }
      
      this.blogpostService.addBlogpost(newBlogpost);

      this.blogPostForm.reset();

    }

  }

}
