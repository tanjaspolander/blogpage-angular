import { Component, inject } from '@angular/core';
import { FormComponent } from './form/form.component';
import { PostsComponent } from './posts/posts.component';
import { BlogpostService } from '../../service/blogpost.service';
import { IBlogpost } from '../../../core/blogpost.model';

@Component({
  selector: 'app-blog-page',
  standalone: true,
  imports: [FormComponent, PostsComponent],
  templateUrl: './blog-page.component.html',
  styleUrl: './blog-page.component.css'
})
export class BlogPageComponent {

blogpostService: BlogpostService = inject(BlogpostService);


blogpostList: IBlogpost[] = [];


ngOnInit(): void {
  this.updateBlogposts();
  this.blogpostService.reload.subscribe(() => this.updateBlogposts());
}

updateBlogposts = () => {
  this.blogpostService.updateBlogpost().then((blogposts: IBlogpost[]) => {
    this.blogpostList = blogposts.reverse();
  });
}


}


