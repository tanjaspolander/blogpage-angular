import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-header-detail',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './header-detail.component.html',
  styleUrl: './header-detail.component.css'
})
export class HeaderDetailComponent {

}
