import { Component } from '@angular/core';
import { BlogPageComponent } from '../blog-page/blog-page.component';
import { HeaderComponent } from '../header/header.component';

@Component({
  selector: 'app-blog-root',
  standalone: true,
  imports: [BlogPageComponent, HeaderComponent],
  templateUrl: './blog-root.component.html',
  styleUrl: './blog-root.component.css'
})
export class BlogRootComponent {

}
