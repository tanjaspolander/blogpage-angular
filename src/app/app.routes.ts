import { Routes } from '@angular/router';
import { BlogPageComponent } from './components/blog-page/blog-page.component';
import { BlogpostDetailComponent } from './components/blogpost-detail/blogpost-detail.component';
import { ChatPageComponent } from './components/chat-page/chat-page.component';
import { ChatRootComponent } from './components/chat-root/chat-root.component';
import { BlogRootComponent } from './components/blog-root/blog-root.component';
import { BlogDetailRootComponent } from './components/blog-detail-root/blog-detail-root.component';

export const routes: Routes = [
    { path: '', component: BlogRootComponent },
    { path: 'blogpost/:id', component: BlogDetailRootComponent },
    { path: 'chat', component: ChatRootComponent },


];
