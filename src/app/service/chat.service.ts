import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IMessage} from '../../core/message.model'


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public messages: IMessage[] = [];

  constructor(private http: HttpClient) { }

  
  addMessage = (username: string, content: string): Observable<any> => {
    return this.http.post('http://localhost:3000/messages', {username, content});
  }
  
  getMessages = (): Observable<any> => {
    return this.http.get('http://localhost:3000/messages');
  }
}

 

