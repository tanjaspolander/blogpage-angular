import { Injectable } from '@angular/core';
import { IBlogpost } from '../../core/blogpost.model';
import { IComment } from '../../core/comment.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlogpostService {

  private blogpostList: IBlogpost[] = [];
  private comments: IComment[] = [];  


  reload = new Subject<void>()


  updateBlogpost = async () => {
    const res = await fetch('http://localhost:3000/blogposts');
    this.blogpostList = await res.json();
    return this.blogpostList;
  }
  
  // UTAN JSON SERVER. BARA PUTTAR IN I LISTAN.
  // addBlogpost = (newBlogpost: IBlogpost) => this.blogpostList.push(newBlogpost);
 
 
 
  addBlogpost = async (newBlogpost: IBlogpost) => {
    
     await fetch('http://localhost:3000/blogposts', {
      method: 'POST',
      body: JSON.stringify(newBlogpost),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    
    this.updateBlogpost();
    console.log('FROM SERVICE - THE UPDATED LIST:', this.blogpostList)
    
    this.reload.next()



  }
  
 

  // getBlogpostById = (id: string) => this.blogpostList.find(blogpost => blogpost.id === id)

  getBlogpostById = async (id: string) => {
  const res = await fetch('http://localhost:3000/blogposts/' + id);
  return await res.json();
  
  }


  // addComment = (id: string, comment: IComment) => {
  //   const blogpost = this.blogpostList.find(blogpost => blogpost.id === id);
  //   if (blogpost) {
  //     blogpost.comments.push(comment);
  //   }
  // }

  addComment = async (id: string, comment: IComment) => {
  const blogpost = await this.getBlogpostById(id);
  if (blogpost) {
    blogpost.comments.push(comment);
    await fetch(`http://localhost:3000/blogposts/${id}`, {
      method: 'PUT',
      body: JSON.stringify(blogpost),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    this.reload.next();
  }
}

  getComments = () => this.comments

  constructor() { }
}
