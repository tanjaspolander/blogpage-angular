/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {},
    fontFamily: {
      signature: ['"Urbanist", sans-serif'],
    },
    colors: {
      black: "#000000",
      white: "#ffffff",
      mustard: '#FFDF6D',
      mint: '#D1FFC6',
      banana: '#FEEEB9',
      curry: '#765C00',
      darkJungle: '#2E2D2D',
    },
    
  },
  plugins: [],
}